'use strict';

class addPost {
    constructor() {
        this.waitForHeadline = '.wp-heading-inline';
        this.viewPost = 'View post';
        this.publish = 'publish';
        this.boldButtonID = 'qt_content_strong';
        this.content = "Test user likes to blog. The content of the blog can be ";
        this.timeOutMessage = 'Element taking too long to appear in the DOM';
        this.newLink = "//span[contains(.,'New')]";
        this.titleID = 'title';
        this.title = 'This is a blog by a test_user';
        this.contentFormID = 'content';
        this.until = protractor.ExpectedConditions;
    }

    createNewBlog() {
        return element(by.xpath(this.newLink)).click()
            .then(() => browser.wait(this.until.presenceOf(element(by.css(this.waitForHeadline))), 5000, this.timeOutMessage));
    }

    addBlogTitle() {
        return element(by.id(this.titleID)).sendKeys(this.title);
    }

    textInBold(text) {
        return element(by.id(this.boldButtonID)).click()
            .then(() => element(by.id(this.contentFormID)).sendKeys(text))
            .then(() => element(by.id(this.boldButtonID)).click());
    }

    addBlogContent() {
        return element(by.id(this.contentFormID)).sendKeys(this.content)
    }

    publishBlog() {
        return element(by.id(this.publish)).click()
            .then(() => browser.wait(this.until.presenceOf(element(by.linkText(this.viewPost))), 15000, this.timeOutMessage))
            .then(() => browser.sleep(5000));
    }

    openBlog(){
        return element(by.css('.entry-title')).click()
    .then(() => browser.wait(protractor.ExpectedConditions.presenceOf(element(by.id('reply-title'))), 5000, this.timeOutMessage))
    }
}

module.exports = new addPost();