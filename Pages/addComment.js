'use strict';

class addComment {
    constructor() {
        this.submit = 'submit';
        this.comment = 'comment';
        this.mailID = 'email';
        this.author = 'author';
    }

    commentDetails(username, mailId, comment) {
        element(by.id(this.author)).clear().sendKeys(username)
            .then(() => element(by.id(this.mailID)).clear().sendKeys(mailId))
            .then(() => element(by.id(this.comment)).clear().sendKeys(comment));
    }

    clickToSubmitComment() {
        return element(by.id(this.submit)).click();
    }
}

module.exports = new addComment();