'use strict';

let utils = require('../Utilities/login');
let blog = require('../Pages/blog');
let addComment = require('../Pages/addComment');

describe('TestCase 3 (Comment tests):', function () {

    beforeAll(function () {
        browser.ignoreSynchronization = true;
        utils.openWebsite('http://ec2-54-90-154-147.compute-1.amazonaws.com')
            .then(() => browser.sleep(3000))
            .then(() => utils.login())
            .then(() => blog.createNewBlog())
            .then(() => blog.addBlogTitle())
            .then(() => blog.addBlogContent())
            .then(() => blog.textInBold('anything.'))
            .then(() => browser.sleep(6000))
            .then(() => blog.publishBlog())
            .then(() => browser.restart());
    });

    it('Add & verify Comment on the Blog by User1', function () {
        browser.ignoreSynchronization = true;
        utils.openWebsite('http://ec2-54-90-154-147.compute-1.amazonaws.com')
            .then(() => blog.openBlog())
            .then(() => addComment.commentDetails('User1', 'user1@xyz.com', 'Comment by User1'))
            .then(() => addComment.clickToSubmitComment())
            .then(() => browser.wait(protractor.ExpectedConditions.presenceOf(element(by.css('.comments-title'))), 5000, 'Element is taking too long to Display'))
            .then(() => {
                expect(element(by.xpath('//b[contains(.,"User1")]')).getText()).toBe('User1');
                expect(element(by.xpath('.//p[contains(.,"Comment by User1")]')).getText()).toBe('Comment by User1');
            })
    });

    it('Add & verify Reply on the Blog by User2', function () {
        browser.ignoreSynchronization = true;
        browser.sleep(3000);
        element(by.css('.reply')).click()
            .then(() => addComment.commentDetails('User2', 'user2@xyz.com', 'Comment by User2'))
            .then(() => browser.sleep(15000))
            .then(() => addComment.clickToSubmitComment())
            .then(() => {
                expect(element(by.xpath('//b[contains(.,"User2")]')).getText()).toBe('User2');
                expect(element(by.xpath('.//p[contains(.,"Comment by User2")]')).getText()).toBe('Comment by User2');
            })
    })
});