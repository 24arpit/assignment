'use strict';

let utils = require('../Utilities/login');
let blog = require('../Pages/blog');

describe('TestCase 2 (Publish tests):', function () {

    describe('Unauthenticated users should be able to see the blog title', function () {
        beforeEach(function () {
            utils.openWebsite('http://ec2-54-90-154-147.compute-1.amazonaws.com')
                .then(() => utils.login())
                .then(() => blog.createNewBlog())
                .then(() => blog.addBlogTitle())
                .then(() => blog.addBlogContent())
                .then(() => blog.textInBold('anything.'))
                .then(() => browser.sleep(6000))
                .then(() => blog.publishBlog())
                .then(() => browser.restart());
        });


        it('Unauthenticated users should be able to see the blog title on the blog index page.', function () {
            browser.ignoreSynchronization = true;
            utils.openWebsite('http://ec2-54-90-154-147.compute-1.amazonaws.com');
            expect(element(by.xpath('//a[contains(.,\'This is a blog by a test_user\')]')).getText()).toBe('This is a blog by a test_user');
        })
    });


    describe('Text should be in bold', function () {
        beforeEach(function () {
            utils.openWebsite('http://ec2-54-90-154-147.compute-1.amazonaws.com')
                .then(() => utils.login())
                .then(() => blog.createNewBlog())
                .then(() => blog.addBlogTitle())
                .then(() => blog.addBlogContent())
                .then(() => blog.textInBold('anything.'))
                .then(() => browser.sleep(6000))
                .then(() => blog.publishBlog())
                .then(() => browser.wait(protractor.ExpectedConditions.presenceOf(element(by.linkText('View post'))), 15000, 'Element taking too long to appear in the DOM'))
                .then(() => element(by.linkText('View post')).click())
        });

        it('anything should be in bold', function () {
            browser.ignoreSynchronization = true;
            utils.openWebsite('http://ec2-54-90-154-147.compute-1.amazonaws.com');
            expect(element(by.xpath('//strong[contains(.,\'anything.\')]')).getText()).toContain('anything');
            browser.restart();

        })
    });
});