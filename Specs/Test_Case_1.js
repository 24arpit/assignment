'use strict';
let utils = require('../Utilities/login');

    describe('TestCase 1 (Trivial Test):', function (){
    it('Verify if site is accessible', function () {

        utils.openWebsite('http://ec2-54-90-154-147.compute-1.amazonaws.com');
        browser.getTitle().then((webPageTitle) => expect(webPageTitle).toContain("user's Blog!"));
    });
});
