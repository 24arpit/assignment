'use strict'

class utilities{
constructor(){

    this.dashBoard = 'save-post';
    this.timeOutMessage = 'Element taking too long to appear in the DOM';
    this.submitButton = 'wp-submit';
    this.userPassword = 'user_pass';
    this.userID = 'user_login';
}

    openWebsite(url){
       return browser.get(url);
    }

    login(){
        return element(by.linkText('Log in')).click()
            .then(() => element(by.id(this.userID)).sendKeys('test_user'))
            .then(() => element(by.id(this.userPassword)).sendKeys('testuser@123'))
            .then(() => element(by.id(this.submitButton)).click())
            .then(() => {
                let until = protractor.ExpectedConditions;
                browser.wait(until.presenceOf(element(by.id(this.dashBoard))), 5000, this.timeOutMessage)
            });
    }
}

module.exports = new utilities();