# README #

This repository contains automated test scenarios for a blog application hosted at: http://ec2-54-90-154-147.compute-1.amazonaws.com 

### What is this repository for? ###

* Protractor is used as the automation tool.
* Automation tests runs on Google Chrome.

### Software Required

* Node
* NPM
* Google Chrome

### How do I get set up? ###

* All the dependencies are mentioned in package.json
* After cloning this repo, navigate to the cloned directory and **run 'npm install'**

### How to Run the Tests
 
1. Open the terminal/command prompt then navigate to project directory
2. Update the Selenium webdriver using : ./node_modules/.bin/webdriver-manager update 
3. Start the Selenium webdriver using: ./node_modules/.bin/webdriver-manager start
4. Open new terminal and again navigate to project directory
5. Run the tests using ./node_modules/.bin/protractor Config/config.js
6. html report is generated and saved in the folder named as 'Screenshots'.

### Who do I talk to? ###

* Repo owner : Arpit Jain [arpitjain10104695@gmail.com]

### Future Work

* Test Data to be extracted out in separate file
* Adding logs to debug the issue/progress of the tests
* Add a afterEach/afterAll method to do the data cleanup